package com.example.task12;

public class Task12Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        /*
        int[] arr = {9, 11, 7, 8};
        selectionSort(arr);
        System.out.println(java.util.Arrays.toString(arr));
         */
    }

    static void selectionSort(int[] arr) {
        if (arr==null || arr.length==0){
            return;
        }
        int j=0;
        while(j!= arr.length-1){
            int min=arr[j],mini=j;
            for (int i=j+1;i< arr.length;i++){
                if (min>=arr[i]){
                    min=arr[i];
                    mini=i;
                }
            }
            arr[mini]=arr[j];
            arr[j]=min;
            j++;
        }
        //todo напишите здесь свою корректную реализацию этого метода, вместо существующей
    }

}