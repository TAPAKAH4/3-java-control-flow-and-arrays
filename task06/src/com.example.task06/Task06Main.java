package com.example.task06;

public class Task06Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        /*
        System.out.println(getMax(1, 2, 3, 4));
         */
    }

    static int getMax(int a, int b, int c, int d) {
        int max1,max2;
        if (a>=b){
            max1=a;
        } else{
            max1=b;
        }
        if (c>=d){
            max2=c;
        } else{
            max2=d;
        }
        if (max1>=max2){
            return max1;
        } else{
            return max2;
        }
    }

}