package com.example.task13;

public class Task13Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        /*
        int[] arr = {9, 1100, 7, 8};
        removeMoreThen1000(arr);
        System.out.println(java.util.Arrays.toString(arr));
         */
    }

    static int[] removeMoreThen1000(int[] arr) {
        if (arr==null){
            return null;
        }
        if ( arr.length==0){
            return new int[]{};
        }
        int newNum= arr.length;
        for (int i=0;i< arr.length;i++){
            if (arr[i]>1000){
                newNum--;
            }
        }
        int[] newarr= new int[newNum];
        int pos=0;
        for(int i=0;i< arr.length;i++) {
            if (arr[i] < 1000) {
                newarr[pos]=arr[i];
                pos++;
            }
        }
        //todo напишите здесь свою корректную реализацию этого метода, вместо существующей
        return  newarr;
    }

}